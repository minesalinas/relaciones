package many.one.c

class FaceC {

	static hasOne = [nose:NoseC]

    static constraints = {
    	nose(nullable: true)
    }
}