package many.one.b



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(FaceBController)
class FaceBControllerTests {

    void testSomething() {
       fail "Implement me"
    }
}
