package many.one.b



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(FaceB)
class FaceBTests {

    void testSomething() {
       fail "Implement me"
    }
}
