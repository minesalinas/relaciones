package many.one.c



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(NoseCController)
class NoseCControllerTests {

    void testSomething() {
       fail "Implement me"
    }
}
